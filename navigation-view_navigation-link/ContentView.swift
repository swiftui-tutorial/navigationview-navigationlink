//
//  ContentView.swift
//  navigation-view_navigation-link
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                NavigationLink("Hello, world!", destination: MyOtherScreen())
                Text("Hello, world!")
                Text("Hello, world!")
                Text("Hello, world!")
            }
            .navigationTitle("All Inboxes")
//            .navigationBarTitleDisplayMode(.inline)
//            .navigationBarHidden(true)
            .navigationBarItems(
                leading: HStack{
                    Image(systemName:"person.fill")
                    Image(systemName:"flame.fill")
                },
                trailing: NavigationLink (
                    destination: MyOtherScreen(), label: {
                        Image(systemName: "gear")
                    }
                ).accentColor(.red)
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct MyOtherScreen: View {
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        ZStack {
            Color.green.edgesIgnoringSafeArea(.all)
                .navigationTitle("Green Screen")
                .navigationBarHidden(true)
            
            VStack {
                Button("BACK BUTTON"){
                    presentationMode.wrappedValue.dismiss()
                }
                
                NavigationLink("Click Here", destination: Text("3rd screen"))
            }
            
            
        }
    }
}
