//
//  navigation_view_navigation_linkApp.swift
//  navigation-view_navigation-link
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

@main
struct navigation_view_navigation_linkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
